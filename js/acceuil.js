$('.__photoo').mouseover(function(){
    $(this).children().children('.__innerText').removeClass("d-none")
}).mouseout(function(){
    $(this).children().children('.__innerText').addClass("d-none");
})

$("#__rse").click(()=>{
    window.location.href = 'notreappui.html'
})

$("#__carriere").click(()=>{
    window.location.href = 'carrieres.html'
})

$("#__propos").click(()=>{
    window.location.href = 'a-propos.html'
})
$("#__imperial").click(()=>{
    window.open('https://www.imperialbrandsplc.com', '_blank');
})

var swiperFull = new Swiper('.swiper-full-screen', {
    loop: true,
    slidesPerView: 1,
    preventClicks: true,
    allowTouchMove: true,
    pagination: {
        el: '.swiper-full-screen-pagination',
        clickable: true
    },
    autoplay: {
        delay: 10000
    },
    keyboard: {
        enabled: true
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    on: {
        resize: function () {
            swiperFull.update();
        }
    }
});

$('.button-next').click(() => {
    swiperFull.slideNext();
})

$('.button-prev').click(() => {
    swiperFull.slidePrev();
})