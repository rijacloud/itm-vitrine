$(document).ready(()=>{
    $('form').submit((e)=>{
        e.preventDefault();
        $("button").click(function(){
            $.post("https://emploi.imperial-tobacco-madagascar.com/index.php/contact",
            {
              name: $('input[name="name"]').val(),
              email: $('input[name="email"]').val(),
              subject: $('input[name="subject"]').val(),
              messages: $('input[name="messages"]').val()
            },
            function(data, status){
              alert("Data: " + data + "\nStatus: " + status);
            });
          });

          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }

          toastr.info("Merci, votre message a été envoyé")
        
    })
})

