var slideRight = {
  distance: "150%",
  origin: "left",
  opacity: null,
};

var slideUp = {
  distance: "150%",
  origin: "bottom",
  opacity: null,
};

var slideDown = {
  distance: "150%",
  origin: "top",
  opacity: null,
};

var slideLeft = {
  distance: "150%",
  origin: "right",
  opacity: null,
};

// ScrollReveal().reveal(".slide-left", slideLeft);
// ScrollReveal().reveal(".slide-up", slideUp);
// ScrollReveal().reveal(".slide-right", slideRight);
// ScrollReveal().reveal(".slide-down", slideDown);

var slideUpDelay = {
  distance: "30%",
  origin: "bottom",
  opacity: null,
  delay: 400,
  duration: 600,
};

var slideLeftDelay = {
  distance: "20%",
  origin: "right",
  opacity: null,
  delay: 400,
  duration: 600,
};

var slideRightDelay = {
  distance: "20%",
  origin: "left",
  opacity: null,
  delay: 400,
  duration: 600,
};

var slideDownDelay = {
  distance: "20%",
  origin: "top",
  opacity: null,
  delay: 400,
  duration: 600,
};

// ScrollReveal().reveal(".slideUpDelay", slideUpDelay);
// ScrollReveal().reveal(".slideLeftDelay", slideLeftDelay);
// ScrollReveal().reveal(".slideRightDelay", slideRightDelay);
// ScrollReveal().reveal(".slideDownDelay", slideDownDelay);

var slideOpacity = {
  opacity: null,
  delay: 400,
  duration: 600,
};

// ScrollReveal().reveal(".slideOpacity", slideOpacity);

$(document).ready(function () {
  var useragent = navigator.userAgent.toLowerCase(); 
  if (useragent.indexOf('safari') != -1) { 
    if (!(useragent.indexOf('chrome') > -1)) {
      $('img.default.logo-dark').css("margin-bottom", "0px");
    }
  }
});

$(window).load(function() {
  $('.loader').addClass('hide')		
})

document.querySelector('.workd').onclick = function(e) {
  e.preventDefault()
  document.querySelectorAll('.flip:not(#workday)').forEach( function(e) {
    e.classList.add('hiden')
  })
  document.getElementById('workday').classList.toggle('hiden')
  document.querySelector('img.rt').classList.remove('rotated')

  this.querySelector('img.rt').classList.toggle('rotated')
}


document.querySelector('.human').onclick = function(e) {
  e.preventDefault()
  document.querySelector('.flip:not(#rha)').classList.add('hiden')
  document.querySelectorAll('.flip:not(#rha)').forEach( function(e) {
    e.classList.add('hiden')
  })
  document.getElementById('rha').classList.toggle('hiden')
  document.querySelector('img.rt').classList.remove('rotated')

  this.querySelector('img.rt').classList.toggle('rotated')
}


document.querySelector('.tabac').onclick = function(e) {
  e.preventDefault()
  document.querySelector('.flip:not(#tabaculture)').classList.add('hiden')
  document.querySelectorAll('.flip:not(#tabaculture)').forEach( function(e) {
    e.classList.add('hiden')
  })
  document.getElementById('tabaculture').classList.toggle('hiden')
  document.querySelector('img.rt').classList.add('rotated-0')

  this.querySelector('img.rt').classList.toggle('rotated')
}


document.querySelector('.distri').onclick = function(e) {
  e.preventDefault()
  document.querySelector('.flip:not(#distribution)').classList.add('hiden')
  document.querySelectorAll('.flip:not(#distribution)').forEach( function(e) {
    e.classList.add('hiden')
  })
  document.getElementById('distribution').classList.toggle('hiden')
  document.querySelector('img.rt').classList.add('rotated-0')
  this.querySelector('img.rt').classList.toggle('rotated')
}


/*document.querySelector('.usine').onclick = function(e) {
  e.preventDefault()
  document.querySelector('.flip:not(#usine)').classList.add('hiden')
  document.querySelectorAll('.flip:not(#usine)').forEach( function(e) {
    e.classList.add('hiden')
  })
  document.getElementById('usine').classList.toggle('hiden')
  document.querySelector('img.rt').classList.remove('rotated')
  this.querySelector('img.rt').classList.toggle('rotated')
}*/


